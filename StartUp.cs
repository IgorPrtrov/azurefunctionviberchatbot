﻿using AzureFunctionViberChatBot.AzureStorage;
using AzureFunctionViberChatBot.Twilio;
using AzureFunctionViberChatBot.Viber;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Build.Framework;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ViberChatbot.Viber;

[assembly: FunctionsStartup(typeof(AzureFunctionViberChatBot.Startup))]
namespace AzureFunctionViberChatBot
{
    public class Startup: FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddLogging();
            builder.Services.AddSingleton<ISettings, Settings>();
            builder.Services.AddSingleton<IViberProcessor, ViberProcessor>();
            builder.Services.AddSingleton<IAzureStorageConfiguration, AzureStorageConfiguration>();
            builder.Services.AddSingleton<IAzureTableHelper, AzureTableHelper>();
            builder.Services.AddSingleton<ITextMessageSender, TextMessageSender>();
            builder.Services.AddSingleton<IViberService, ViberService>();
        }
    }
}
