﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using ViberChatbot.Viber;

namespace AzureFunctionViberChatBot.AzureFunctions
{
    public class ViberFunctions
    {
        private readonly IViberProcessor _viberProcessor;

        public ViberFunctions(IViberProcessor viberProcessor)
        {
            _viberProcessor = viberProcessor;
        }

        [FunctionName("QueueTrigger")]
        public void QueueTrigger([QueueTrigger("viber-bot", Connection = "StorageConnectionString")] string myQueueItem, ILogger log)
        {
            try
            {
                var incoming = JsonConvert.DeserializeObject<ViberIncoming>(myQueueItem);

                log.LogInformation($"EVENT: {incoming.Event}, Sender.Id: {incoming.Sender?.Id}, Sender.Name: {incoming.Sender?.Name}, " +
                    $"Message text: {incoming.Message?.Text}, Message type: {incoming.Message?.Type}");

                _viberProcessor.Process(incoming);
            }
            catch(Exception ex)
            {
                log.LogWarning($"ERROR:: {ex.Message}");
            }
        }
    }
}
