using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using System;
using ViberChatbot.Viber;
using Newtonsoft.Json;

namespace AzureFunctionViberChatBot.AzureFunctions
{
    public class QueueFunction
    {
        [FunctionName("ViberChatBotAzureFunction")]
        [return: Queue("viber-bot", Connection = "StorageConnectionString")]
        public string Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequestMessage req,
            ILogger log)
        {
            try
            {
                var incoming = req.Content.ReadAsAsync<ViberIncoming>().Result;
                var json = JsonConvert.SerializeObject(incoming);

                return json;
            }
            catch(Exception ex)
            {
                log.LogWarning($"ERROR:: {ex.Message}");
                return ex.Message;
            }    
        }
    }
}
