﻿using RestSharp;
using AzureFunctionViberChatBot;
using AzureFunctionViberChatBot.Viber;
using AzureFunctionViberChatBot.AzureStorage;
using System.Linq;
using System;

namespace ViberChatbot.Viber
{
    public class ViberProcessor: IViberProcessor
    {
        private readonly ISettings _settings;
        private readonly IViberService _viberService;

        public ViberProcessor(ISettings settings, IViberService viberService)
        {
            _settings = settings;
            _viberService = viberService;
        }

        public void Process(ViberIncoming incoming)
        {
            if (incoming?.Message?.Type == "text")
            {
                ProcessMessage(incoming);
            }
            else if(incoming?.Message?.Type != null && incoming?.Sender?.Id != null)
            {
                SendTextMessage("We support only text messages", incoming?.Sender?.Id);
            }
        }

        private void ProcessMessage(ViberIncoming incoming)
        {
            var role = _viberService.GetRole(incoming);


            if (role == ERole.Admin)            
                AdminRespond(incoming);
            

            if (role == ERole.Pastor)           
                PastorRespond(incoming);
            

            if (role == ERole.Member)            
                MemberRespond(incoming);
            

            if (role == ERole.UnAuthorized)           
                UnAuthorizedRespond(incoming);          
        }


        private void SendTextMessage(string message, string senderId)
        {
            var client = new RestClient("https://chatapi.viber.com/pa/send_message");
            var request = new RestRequest(RestSharp.Method.POST);
            request.AddJsonBody(new
            {
                receiver = senderId,    // receiver	(Unique Viber user id, required)
                type = "text",          // type	(Message type, required) Available message types: text, picture, etc
                text = message
            });
            request.AddHeader("X-Viber-Auth-Token", _settings.ViberKey);
            client.Execute(request);
        }

        private void AdminRespond(ViberIncoming incoming)
        {
            if (incoming.Message.Text.ToLower().Contains("AddChurch".ToLower()))
            {
                var words = incoming.Message.Text.Split(" ");
                var nameOfChurch = words.Count() == 5 ? words[2] : string.Empty;
                var language = words.Count() == 5 ? words[4] : string.Empty;

                if (language != ELanguage.Russian.ToString() && language != ELanguage.English.ToString())
                {
                    SendTextMessage("Please enter correct name of language (Russian or English).", incoming.Sender.Id);
                    return;
                }

                if (!_viberService.GetMembersLinks(nameOfChurch).Any() && words.Count() == 5)
                {
                    _viberService.AddNewChurch(nameOfChurch, words[3], words[4]);
                    var linksToSendPastor = _viberService.GetMembersLinks(nameOfChurch);
                    _viberService.SendMembersLinksToPastor(linksToSendPastor, words[3], words[4]);

                    SendTextMessage("The messages have been sent successfully", incoming.Sender.Id);
                }
                else
                {
                    SendTextMessage("The messages have been  already sent or try to use other name of church. " +
                        "Pattern is \"{admin code} {AddChurch} {name of church} {pastor phone number} {Russian or English}\"", incoming.Sender.Id);
                }
            }
            else if (!incoming.Message.Text.ToLower().Contains("SendInvitationMessages".ToLower()))
                SendTextMessage("Please send \"{admin code} {AddChurch} {name of church} {pastor phone number} {Russian or English}\" to execute this command, no space", incoming.Sender.Id);
        }

        private void PastorRespond(ViberIncoming incoming)
        {
            var nickname = incoming.Message.Text.Replace("\r", " ").Replace("\n", " ").Contains(" ")
                            ? incoming.Message.Text.Replace("\r", " ").Replace("\n", " ").Split(" ")[0]
                            : incoming.Message.Text;

            var user = _viberService.GetUser(incoming.Sender.Id);

            var viberId = _viberService.GetViberIdBy(nickname.ToUpper(), user.NameOfChurch);

            if (string.IsNullOrEmpty(viberId))
            {
                SendTextMessage(user.Language == ELanguage.English.ToString()
                    ?
                    $"Can't recognize the nickname. Please start your message with the nickname of the member (Example: ANDERSON ) " +
                    $"or maybe the member is UnAuthorized. Example:{Environment.NewLine}{Environment.NewLine}" +
                    $"ANDERSON {Environment.NewLine}I glad to help you. Give me more information about your problem"
                    :
                    $"Не могу распознать псевдоним. " +
                    $"Пожалуйста, начните свое сообщение с псевдонима участника (пример: АНДЕРСОН) или, возможно, участник является неавторизованным. " +
                    $"Пример:{Environment.NewLine}{Environment.NewLine}" +
                    $"АНДЕРСОН {Environment.NewLine}Рад помочь вам. Расскажите подробнее о своей проблеме.",

                    incoming.Sender.Id);
            }
            else
            {
                var message = incoming.Message.Text.Replace(nickname,
                            user.Language == ELanguage.English.ToString() ? "Pastor: " : "Пастор: ");

                if (!string.IsNullOrEmpty(message.Trim()))
                {
                    _viberService.UpdateMessagesHistory(viberId, message);

                    SendTextMessage(message, viberId);
                    SendTextMessage(user.Language == ELanguage.English.ToString()
                        ? "The message has been sent successfully."
                        : "Сообщение было успешно отправлено",
                        incoming.Sender.Id);

                    _viberService.UpdateOutgoingMessages(incoming.Sender.Id);
                }
                else
                    SendTextMessage(user.Language == ELanguage.English.ToString()
                            ? "Your message must contain text other than a nickname"
                            : "Ваше сообщение должно содержать текст кроме псевдонима", incoming.Sender.Id);
            }
        }

        private void MemberRespond(ViberIncoming incoming)
        {
            var questionMark = incoming.Message.Text.Substring(0, 1);
            var user = _viberService.GetUser(incoming.Sender.Id);

            if (questionMark == "?")
            {
                var pastorViberId = _viberService.GetPastorViberId(user.NameOfChurch);

                if (string.IsNullOrEmpty(pastorViberId))
                {
                    SendTextMessage(user.Language == ELanguage.English.ToString()
                        ? "Can't find the Pastor in your church or maybe he is UnAuthorized."
                        : "Не могу найти пастора в вашей церкви или, может быть, он не авторизован.", incoming.Sender.Id);
                }
                else
                {
                    var message = incoming.Message.Text.Substring(1).Trim();
                    if (!string.IsNullOrEmpty(message))
                    {
                        var nickname = _viberService.GetnicknameBy(incoming.Sender.Id);
                        message = user.Language == ELanguage.English.ToString()
                            ? $"{nickname}:{Environment.NewLine}{message}"
                            : $"{nickname}:{Environment.NewLine}{message}";

                        _viberService.UpdateMessagesHistory(incoming.Sender.Id, message);
                        message = _viberService.GetLatestMessageHistory(incoming.Sender.Id);

                        SendTextMessage(message, pastorViberId);
                        SendTextMessage(user.Language == ELanguage.English.ToString()
                            ? "The anonymous message has been sent successfully to the Pastor."
                            : "Анонимное сообщение успешно отправлено пастору.", incoming.Sender.Id);

                        _viberService.UpdateOutgoingMessages(incoming.Sender.Id);
                    }
                    else
                        SendTextMessage(user.Language == ELanguage.English.ToString()
                            ? "Your message must have text except '?'"
                            : "Ваше сообщение должно содержать текст, кроме '?'", incoming.Sender.Id);
                }
            }
            else
            {
                SendTextMessage(user.Language == ELanguage.English.ToString()
                    ?
                    "To send anonymous messages to the Pastor please start your message with '?'. " +
                    $"Example:{Environment.NewLine}{Environment.NewLine}" +
                    $"? {Environment.NewLine}Hi! I have one question."
                    :
                    "Чтобы отправлять анонимные сообщения пастору, пожалуйста, начните свое сообщение с '?'. " +
                    $"Пример:{Environment.NewLine}{Environment.NewLine}" +
                    $"? {Environment.NewLine}Здравствуйте! У меня есть один вопрос."
                    , incoming.Sender.Id);
            }
        }

        private void UnAuthorizedRespond(ViberIncoming incoming)
        {
            var codeAuthentication = incoming.Message.Text;
            if (string.IsNullOrEmpty(codeAuthentication))
            {
                SendTextMessage("Hi, sorry but I can't recognize you. Please ask your Pastor to invite you to this chat. " +
                    $"You have to receive a link with code authentication. If you already have it just go to the link and submit the code. Thanks.{Environment.NewLine}{Environment.NewLine}" +
                    "Привет, извини, но я не могу тебя распознать. Пожалуйста, попросите вашего пастора пригласить вас в этот чат. Вы должны получить ссылку с кодом. " +
                    "Если он у вас уже есть, просто перейдите по ссылке и введите код. Спасибо.", incoming.Sender.Id);
            }
            else
            {
                var result = _viberService.IsVerifiedUser(incoming.Sender.Id, codeAuthentication);
                if (result == EVerifiedUser.Success)
                {
                    var roleAfterVirification = _viberService.GetRole(incoming);
                    var user = _viberService.GetUser(incoming.Sender.Id);

                    if (roleAfterVirification == ERole.Member)
                        SendTextMessage(user.Language == ELanguage.English.ToString()
                            ?
                            "You have been verified successfully as a member of the church. Now you can send anonymous messages to Pastor. " +
                            "To send an anonymous messages to the Pastor please start your message with '?'." +
                            $"Example:{Environment.NewLine}{Environment.NewLine}" +
                            $"? {Environment.NewLine}Hi! I have one question."
                            :
                            "Вы успешно прошли проверку как член церкви. Теперь вы можете отправлять анонимные сообщения пастору. " +
                             "Чтобы отправлять анонимные сообщения пастору, пожалуйста, начните свое сообщение с '?'. " +
                            $"Пример:{Environment.NewLine}{Environment.NewLine}" +
                            $"? {Environment.NewLine}Здравствуйте! У меня есть один вопрос."
                            , incoming.Sender.Id);

                    if (roleAfterVirification == ERole.Pastor)
                        SendTextMessage(user.Language == ELanguage.English.ToString()
                               ?
                              "You have been verified successfully as the Pastor. Now you can respond to anonymous messages from members of your church. " +
                               $"To respond to anonymous messages please start your message with the nickname of the member. " +
                               $"Example:{Environment.NewLine}{Environment.NewLine}" +
                               $"ANDERSON {Environment.NewLine}I glad to help you. Give me more information about your problem"
                               :
                               "Вы успешно прошли проверку как пастор. Теперь вы можете отвечать на анонимные сообщения от членов вашей церкви. " +
                                $"Чтобы отвечать на анонимные сообщения, начните свое сообщение с псевдонима члена церкви. " +
                                $"Пример:{Environment.NewLine}{Environment.NewLine}" +
                                $"АНДЕРСОН {Environment.NewLine}Рад помочь вам. Расскажите подробнее о своей проблеме."
                                , incoming.Sender.Id);
                }
                else if (result == EVerifiedUser.Faild)
                {
                    SendTextMessage("Hi, sorry but I can't recognize you. Please ask your Pastor to invite you to this chat. " +
                    $"You have to receive a link with code authentication. If you already have it just go to the link and submit the code. Thanks.{Environment.NewLine}{Environment.NewLine}" +
                    "Привет, извини, но я не могу тебя распознать. Пожалуйста, попросите вашего пастора пригласить вас в этот чат. Вы должны получить ссылку с кодом. " +
                    "Если он у вас уже есть, просто перейдите по ссылке и введите код. Спасибо.", incoming.Sender.Id);
                }
            }
        }
    }

    public interface IViberProcessor
    {
        void Process(ViberIncoming incoming);
    }
}