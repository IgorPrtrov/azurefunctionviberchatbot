﻿using AzureFunctionViberChatBot.AzureStorage;
using AzureFunctionViberChatBot.Twilio;
using Microsoft.Azure.Storage.Blob.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using ViberChatbot.Viber;

namespace AzureFunctionViberChatBot.Viber
{
    public class ViberService: IViberService
    {
        private readonly IAzureTableHelper _azureTableHelper;
        private readonly ISettings _settings;
        private readonly ITextMessageSender _textMessageSender;
        public ViberService(IAzureTableHelper azureTableHelper, ISettings settings, ITextMessageSender textMessageSender)
        {
            _azureTableHelper = azureTableHelper;
            _settings = settings;
            _textMessageSender = textMessageSender;
            _azureTableHelper.Initialize();
        }

        public void AddNewChurch(string nameOfChurch, string pastorPhoneNumber, string language)
        {
            _azureTableHelper.AddOrUpdateMember(new AzureMember
            {
                AddedDate = DateTime.Now.ToUniversalTime(),
                NameOfChurch = nameOfChurch,
                Role = ERole.Pastor.ToString(),
                PastorPhoneNumber = pastorPhoneNumber,
                Language = language
            });

            _azureTableHelper.AddOrUpdateMember(new AzureMember
            {
                AddedDate = DateTime.Now.ToUniversalTime(),
                NameOfChurch = nameOfChurch,
                Role = ERole.Member.ToString(),
                PastorPhoneNumber = string.Empty,
                Language = language
            });
        }

        public Dictionary<string, string> GetMembersLinks(string nameOfChurch)
        {
            var members = _azureTableHelper.GetByPropertyNameAndValueForMember(Constants.AzureMember.NameOfChurch, nameOfChurch);
            var result = new Dictionary<string, string>();

            foreach(var member in members)
            {
                result.Add(member.Role, $"viber://pa?chatURI=cnfsn&text={member.PartitionKey}");
            }

            return result;
        }

        public void SendMembersLinksToPastor(Dictionary<string, string> membersLinks, string pastorPhoneNumber, string language)
        {
            foreach(var memberLink in membersLinks)
            {
                if(memberLink.Key == ERole.Pastor.ToString())
                {
                    var linkForPastor = memberLink.Value;
                    _textMessageSender.SendMessage(new SendMessageDto
                    {
                        NumberTo = pastorPhoneNumber,
                        Body = language == ELanguage.English.ToString() 
                                ?
                                $"For Pastor. Please go to the link {linkForPastor} " +
                                $"and fill in your code authentication " +
                                $"to start using this app as Pastor."
                                :
                                $"Для пастора. Перейдите по ссылке {linkForPastor}" +
                                 $" и введите свой код аутентификации" +
                                 $", чтобы начать использовать это приложение в качестве пастора."
                    });
                }

                if (memberLink.Key == ERole.Member.ToString())
                {
                    var linkForMembers = memberLink.Value;

                    _textMessageSender.SendMessage(new SendMessageDto
                    {
                        NumberTo = pastorPhoneNumber,
                        Body = language == ELanguage.English.ToString() 
                                ?
                                $"For Church Members. To start using Spiritual support go to the link " +
                                $"{linkForMembers} and fill in your church code authentication. " +
                                $"How it works: http://eltan-inc.com"
                                :
                                $"Для членов Церкви. Чтобы начать пользоваться Духовной поддержкой, перейдите по ссылке " +
                                 $"{linkForMembers} и введите свой код аутентификации. " +
                                 $"Как это работает: http://eltan-inc.com"
                    });
                }
            }
        }

        public EVerifiedUser IsVerifiedUser(string viberSenderId, string codeAuthentication)
        {
            var member = _azureTableHelper.GetByPropertyNameAndValueForMember(Constants.AzureMember.PartitionKey, codeAuthentication)
                .FirstOrDefault();

            if(member == null)
                return EVerifiedUser.Faild;

            var existingNickNames = _azureTableHelper.GetByPropertyNameAndValue(Constants.AzureUser.NameOfChurch, member.NameOfChurch)
                .Select(c => c.NickName);

            string uniqueNickName = string.Empty;

            if (member.Language == ELanguage.Russian.ToString())
                uniqueNickName = GetRandomNickName(_settings.RussianLastNames, existingNickNames.ToList());

            if (member.Language == ELanguage.English.ToString())
                uniqueNickName = GetRandomNickName(_settings.EnglishLastNames, existingNickNames.ToList());

            var pastorId = GetPastorViberId(member.NameOfChurch);

            if(!string.IsNullOrEmpty(pastorId) && member.Role == ERole.Pastor.ToString())
                return EVerifiedUser.Faild;

            _azureTableHelper.AddOrUpdateUser(new AzureUser()
            {
                NickName = uniqueNickName,
                Role = member.Role,
                NameOfChurch = member.NameOfChurch,
                ViberSenderId = viberSenderId,
                DateTimeVerification = DateTime.Now.ToUniversalTime(),
                Language = member.Language,
                MessagesHistory = string.Empty
            });

            return EVerifiedUser.Success;
        }

        private string GetRandomNickName(string[] lastNames, List<string> existingNickNames)
        {
            Random r = new Random();
            while (true)
            {
                int rInt = r.Next(0, lastNames.Length - 1);
                var nikeName = lastNames[rInt];

                if (existingNickNames.All(c => c.ToUpper() != nikeName.ToUpper()))
                    return nikeName;
            }
        }

        public string GetPastorViberId(string nameOfChurch)
        {          
            return _azureTableHelper.GetPastorIdBy(nameOfChurch)?.ViberSenderId;          
        }

        public ERole GetRole(ViberIncoming incoming)
        {
            if (incoming.Message.Text.Contains(_settings.AdminCode))            
                return ERole.Admin;

            var user = _azureTableHelper.GetByPropertyNameAndValue(Constants.AzureUser.ViberSenderId, incoming.Sender.Id);
            if (user.Any())
                return (ERole)Enum.Parse(typeof(ERole), user.FirstOrDefault()?.Role);

            return ERole.UnAuthorized;
        }

        public string GetViberIdBy(string nickName, string nameOfChurch)
        {
            var user = _azureTableHelper.GetUserBy(nickName, nameOfChurch);
            return user?.ViberSenderId;
        }

        public AzureUser GetUser(string viberSenderId)
        {
            return _azureTableHelper.GetByPropertyNameAndValue(Constants.AzureUser.ViberSenderId, viberSenderId)?.FirstOrDefault();
        }

        public string GetnicknameBy(string viberSenderId)
        {
            return _azureTableHelper.GetByPropertyNameAndValue(Constants.AzureUser.ViberSenderId, viberSenderId)?.FirstOrDefault()?.NickName;
        }

        public void UpdateOutgoingMessages(string viberSenderId)
        {
            var user = _azureTableHelper.GetByPropertyNameAndValue(Constants.AzureUser.ViberSenderId, viberSenderId)?.FirstOrDefault();

            if(user != null)
            {
                _azureTableHelper.UpdateOutgoingMessages(user);
            }
        }

        public void UpdateMessagesHistory(string viberSenderId, string message)
        {
            var user = GetUser(viberSenderId);
            if(user != null)
            {
                user.MessagesHistory += string.IsNullOrEmpty(user.MessagesHistory) ? message : $"{Environment.NewLine}{Environment.NewLine}{message}";
                _azureTableHelper.AddOrUpdateUser(user);
            }
        }

        public string GetLatestMessageHistory(string viberSenderId)
        {
            var user = GetUser(viberSenderId);
            if (user != null)
            {
                if(user.MessagesHistory.Length >= 6800)
                {
                    var message = user.MessagesHistory.Substring(user.MessagesHistory.Length - 6800);
                    message = "..." + message.Substring(3);

                    return message;
                }

                return user.MessagesHistory;
            }

            return string.Empty;
        }
    }

    public interface IViberService 
    {
        EVerifiedUser IsVerifiedUser(string viberSenderId, string codeAuthentication);
        string GetPastorViberId(string nameOfChurch);
        ERole GetRole(ViberIncoming incoming);
        string GetViberIdBy(string nickName, string nameOfChurch);
        AzureUser GetUser(string viberSenderId);
        string GetnicknameBy(string viberSenderId);
        void AddNewChurch(string nameOfChurch, string pastorPhoneNumber, string language);
        Dictionary<string, string> GetMembersLinks(string nameOfChurch);
        void SendMembersLinksToPastor(Dictionary<string, string> membersLinks, string pastorPhoneNumber, string language);
        void UpdateOutgoingMessages(string viberSenderId);
        void UpdateMessagesHistory(string viberSenderId, string message);
        string GetLatestMessageHistory(string viberSenderId);
    }
}
