﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AzureFunctionViberChatBot.AzureStorage
{
    public class Constants
    {
        public class AzureUser
        {
            public const string ViberSenderId = "ViberSenderId";
            public const string NickName = "NickName";
            public const string PhoneNumber = "PhoneNumber";
            public const string Role = "Role";
            public const string NameOfChurch = "NameOfChurch";
        }

        public class AzureMember
        {
            public const string PartitionKey = "PartitionKey";
            public const string PastorPhoneNumber = "PastorPhoneNumber";
            public const string Role = "Role";
            public const string NameOfChurch = "NameOfChurch";
        }
    }
}
