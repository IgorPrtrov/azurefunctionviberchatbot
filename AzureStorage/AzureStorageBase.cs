﻿using Microsoft.WindowsAzure.Storage;
using System;

namespace AzureFunctionViberChatBot.AzureStorage
{
    public class AzureStorageBase
    {
        private const string ConnectionTemplate = "DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}";
        public readonly IAzureStorageConfiguration StorageConfiguration;
        private CloudStorageAccount _azureAccount;

        public AzureStorageBase(IAzureStorageConfiguration storageConfiguration)
        {
            StorageConfiguration = storageConfiguration;
        }

        public CloudStorageAccount AzureStorageAccount
        {
            get
            {
                if (_azureAccount != null)
                    return _azureAccount;

                if (StorageConfiguration?.AccountName == null || StorageConfiguration?.AccessKey == null)
                    throw new InvalidOperationException("can not get azure connection string from config");

                var connectionString = string.Format(ConnectionTemplate, StorageConfiguration.AccountName, StorageConfiguration.AccessKey);

                _azureAccount = CloudStorageAccount.Parse(connectionString);
                return _azureAccount;
            }
        }
    }
}
