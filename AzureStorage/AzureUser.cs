﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace AzureFunctionViberChatBot.AzureStorage
{
    public class AzureUser: TableEntity
    {
        public AzureUser()
        {
            var guid = Guid.NewGuid().ToString();
            PartitionKey = guid;
            RowKey = guid.Substring(24);
        }

        public string ViberSenderId { get; set; }
        public string NickName { get; set; }
        public string Role { get; set; }
        public DateTime? DateTimeVerification { get; set; }
        public string NameOfChurch { get; set; }
        public int? OutgoingMessages { get; set; }
        public string Language { get; set; }
        public string MessagesHistory { get; set; }
    }

    public enum ERole
    {
        Pastor,
        Member,
        Admin,
        UnAuthorized
    }

    public enum EVerifiedUser
    {
        Success,
        Faild
    }

    public enum ELanguage
    {
        Russian,
        English
    }
}
