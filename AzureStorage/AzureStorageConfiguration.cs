﻿using System;

namespace AzureFunctionViberChatBot.AzureStorage
{
    public class AzureStorageConfiguration : IAzureStorageConfiguration
    {
        private static readonly Lazy<IAzureStorageConfiguration> ConfigSection = new Lazy<IAzureStorageConfiguration>(LoadFromEnvironmentVariables);
        private static IAzureStorageConfiguration LoadFromEnvironmentVariables()
        {
            return new MixAzureStorageConfiguration()
            {
                AccountName = Environment.GetEnvironmentVariable("AccountName"),
                AccessKey = Environment.GetEnvironmentVariable("AccountKey"),
                UserTbl = Environment.GetEnvironmentVariable("UserTbl"),
                MemberTbl = Environment.GetEnvironmentVariable("MemberTbl")
            };
        }

        public string AccountName => ConfigSection.Value.AccountName;
        public string AccessKey => ConfigSection.Value.AccessKey;
        public string UserTbl => ConfigSection.Value.UserTbl;
        public string MemberTbl => ConfigSection.Value.MemberTbl;
    }

    public class MixAzureStorageConfiguration : IAzureStorageConfiguration
    {
        public string AccountName { get; set; }
        public string AccessKey { get; set; }
        public string UserTbl { get; set; }
        public string MemberTbl { get; set; }
    }

    public interface IAzureStorageConfiguration
    {
        string AccountName { get; }
        string AccessKey { get; }
        string UserTbl { get; }
        string MemberTbl { get; }
    }
}
