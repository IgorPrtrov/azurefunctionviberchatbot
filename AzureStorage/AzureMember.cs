﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace AzureFunctionViberChatBot.AzureStorage
{
    public class AzureMember : TableEntity
    {
        public AzureMember()
        {
            var guid = Guid.NewGuid().ToString();
            PartitionKey = guid;
            RowKey = guid.Substring(24);
        }

        public string NameOfChurch { get; set; }
        public string Role { get; set; }
        public string PastorPhoneNumber { get; set; }
        public DateTime? AddedDate { get; set; }
        public string Language { get; set; }
    }
}
