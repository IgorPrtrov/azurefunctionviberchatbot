﻿using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Linq;

namespace AzureFunctionViberChatBot.AzureStorage
{
    public class AzureTableHelper : AzureStorageBase, IAzureTableHelper
    {
        private readonly Dictionary<string, CloudTable> _tables;
        private CloudTableClient _client;
        private CloudTableClient Client => _client ?? (_client = AzureStorageAccount.CreateCloudTableClient());

        private CloudTable GetTable(string tableName)
        {
            if (!_tables.ContainsKey(tableName))
            {
                var table = Client.GetTableReference(tableName);
                _tables[tableName] = table.ExistsAsync().Result ? table : null;
            }

            return _tables[tableName];
        }

        public AzureTableHelper(IAzureStorageConfiguration storageConfiguration) : base(storageConfiguration)
        {
            _tables = new Dictionary<string, CloudTable>();
        }

        public void AddOrUpdateMember(AzureMember azureMember)
        {
            var table = GetTable(StorageConfiguration.MemberTbl);
            var insertOrMergeOperation = TableOperation.InsertOrMerge(azureMember);
            table.ExecuteAsync(insertOrMergeOperation).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public void AddOrUpdateUser(AzureUser azureUser)
        {
            var table = GetTable(StorageConfiguration.UserTbl);
            var insertOrMergeOperation = TableOperation.InsertOrMerge(azureUser);
            table.ExecuteAsync(insertOrMergeOperation).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public List<AzureMember> GetByPropertyNameAndValueForMember(string propertyName, string value)
        {
            var talbe = GetTable(StorageConfiguration.MemberTbl);
            var query = new TableQuery<AzureMember>()
                .Where(TableQuery.GenerateFilterCondition(propertyName, QueryComparisons.Equal, value));

            var result = talbe.ExecuteQuerySegmentedAsync(query, null).Result.ToList();

            return result;
        }

        public List<AzureUser> GetByPropertyNameAndValue(string propertyName, string value)
        {
            var talbe = GetTable(StorageConfiguration.UserTbl);
            var query = new TableQuery<AzureUser>()
                .Where(TableQuery.GenerateFilterCondition(propertyName, QueryComparisons.Equal, value));

            var result = talbe.ExecuteQuerySegmentedAsync(query, null).Result.ToList();

            return result;
        }

        public AzureUser GetPastorIdBy(string nameOfChurch)
        {
            var query1 = TableQuery.GenerateFilterCondition(Constants.AzureUser.Role, QueryComparisons.Equal, ERole.Pastor.ToString());
            var query2 = TableQuery.GenerateFilterCondition(Constants.AzureUser.NameOfChurch, QueryComparisons.Equal, nameOfChurch);
            var totalQuery = TableQuery.CombineFilters(query1, TableOperators.And, query2);

            var talbe = GetTable(StorageConfiguration.UserTbl);
            var query = new TableQuery<AzureUser>().Where(totalQuery);

            var result = talbe.ExecuteQuerySegmentedAsync(query, null).Result.FirstOrDefault();

            return result;
        }

        public AzureUser GetUserBy(string nickName, string nameOfChurch)
        {
            var query1 = TableQuery.GenerateFilterCondition(Constants.AzureUser.NameOfChurch, QueryComparisons.Equal, nameOfChurch);
            var query2 = TableQuery.GenerateFilterCondition(Constants.AzureUser.NickName, QueryComparisons.Equal, nickName);
            var totalQuery = TableQuery.CombineFilters(query1, TableOperators.And, query2);

            var talbe = GetTable(StorageConfiguration.UserTbl);
            var query = new TableQuery<AzureUser>().Where(totalQuery);
                 
            var result = talbe.ExecuteQuerySegmentedAsync(query, null).Result.FirstOrDefault();

            return result;
        }

        public void Initialize()
        {
            var table = Client.GetTableReference(StorageConfiguration.UserTbl);
            table.CreateIfNotExistsAsync().ConfigureAwait(false).GetAwaiter().GetResult();

            table = Client.GetTableReference(StorageConfiguration.MemberTbl);
            table.CreateIfNotExistsAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public void UpdateOutgoingMessages(AzureUser user)
        {
            if (user.OutgoingMessages == null)
                user.OutgoingMessages = 1;
            else
                user.OutgoingMessages += 1;

            AddOrUpdateUser(user);
        }
    }

    public interface IAzureTableHelper
    {
        void AddOrUpdateUser(AzureUser azureUser);
        List<AzureUser> GetByPropertyNameAndValue(string propertyName, string value);
        void Initialize();
        AzureUser GetPastorIdBy(string nameOfChurch);
        AzureUser GetUserBy(string nickName, string nameOfChurch);
        void AddOrUpdateMember(AzureMember azureMember);
        List<AzureMember> GetByPropertyNameAndValueForMember(string propertyName, string value);
        void UpdateOutgoingMessages(AzureUser user);
    }
}
