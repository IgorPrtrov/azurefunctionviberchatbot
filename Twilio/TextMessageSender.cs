﻿using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace AzureFunctionViberChatBot.Twilio
{
    public class TextMessageSender: ITextMessageSender
    {
        private readonly ISettings _settings;
        public TextMessageSender(ISettings settings)
        {
            _settings = settings;
            TwilioClient.Init(settings.TwilioAccountSit, settings.TwilioAuthToken);
        }

        public void SendMessage(SendMessageDto dto)
        {
            MessageResource.Create(
                from: new PhoneNumber(_settings.TwilioNumberFrom),
                to: new PhoneNumber(dto.NumberTo),
                body: dto.Body);
        }
    }

    public interface ITextMessageSender
    {
        void SendMessage(SendMessageDto dto);
    }

    public class SendMessageDto
    {
        public string NumberTo { get; set; }
        public string Body { get; set; }
    }
}
